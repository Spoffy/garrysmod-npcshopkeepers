--[[This file contains all the functions for the creation and dispensing of items.]]--

include("custom_spawns.lua")

util.AddNetworkString("npc_shop_dispense")

function ENT:InitializeShop()
	self.ShopStockList = {{"sent_ball", "models/Combine_Helicopter/helicopter_bomb01.mdl",30}}
end

function ENT:GetShopName()
	return self.shop_name
end

function ENT:SetShopName(name)
	self.shop_name = name
end

function ENT:SetSellList(list)
	if not list then return end
	for _,data in pairs(list) do
		if #data ~= 3 then ErrorNoHalt("NPC SHOP ERROR: Invalid Sales List") return end
	end
	self.ShopStockList = list
end

function ENT:GetSellList()
	return self.ShopStockList
end

function ENT:ClearSellList()
	self.ShopStockList = {}
end

function ENT:AddToSellList(item)
	if not item then return end
	if #item ~= 3 then ErrorNoHalt("NPC SHOP ERROR: Adding invalid item! Format: {class,model,price}") return end
	table.insert(self.ShopStockList,item)
	return true
end

function ENT:RemoveFromSellList(item)
	if not item then ErrorNoHalt("NPC SHOP ERROR: Invalid Removal, not enough args specified. Format: {class,model} or {class}") return end
	if #self:GetSellList() == 0 then ErrorNoHalt("NPC SHOP ERROR: Removal attempted on empty shop list") return end
	local match_models = 0
	if item[2] then match_models = 1 end
	for index,data in pairs(self:GetSellList()) do
		if data[1] == item[1] then
			if match_models and data[2] == item[2] then
				table.remove(self:GetSellList(),index)
				return data
			end
			table.remove(self:GetSellList(), index)
			return data
		end
	end
	
	return nil
end
	
function ENT:GetDispenseSpawnLoc()
	local pos
	if self.DispenseSpawnPoint then
		pos = self.DispenseSpawnPoint
	else
		pos = self:GetPos()
		pos.z = pos.z + 100
	end
	return pos
end

function ENT:SetDispenseSpawnLoc(loc)
	self.DispenseSpawnPoint = loc
	return loc
end

function ENT:DispensePropPhysics(model,ply)
	local ent = ents.Create("prop_physics")
	if not ent then ErrorNoHalt("NPC SHOP ERROR: Unable to spawn entity. Line 61") return end
	ent:SetModel(model)
	ent:SetPos(self:GetDispenseSpawnLoc())
	ent.Owner = ply
	ent:Spawn()
	ent:Activate()
end

function ENT:DispenseEntity(class,model,ply)
	if self:CustomSpawn(class,model,ply) then return end --If the entity has a customised spawn function.
	local ent = ents.Create(class)
	if not ent then ErrorNoHalt("NPC SHOP ERROR: Unable to spawn entity. Line 70") return end
	ent:SetPos(self:GetDispenseSpawnLoc())
	ent:Spawn()
	ent:Activate()
end

function ENT:HandleDispenseRequest(client,class,model)
	if not client or not class or not model then return end
	local price = nil
	for _,entry in pairs(self:GetSellList()) do
		if entry[1] == class and entry[2] == model then
			price = entry[3]
			break
		end
	end
	if price == nil then ErrorNoHalt("NPC SHOP ERROR: Unable to locate item " .. class .. " " .. model) return end
	if(GAMEMODE_NAME == "darkrp") then --DARK RP SPECIFIC STUFF
		client:AddMoney(-price)
		GAMEMODE:Notify(client,0,4,"You bought a " .. class .. " for $" .. price)
	end
	if class == "prop_physics" then
		self:DispensePropPhysics(model,client)
	else
		self:DispenseEntity(class,model,client)
	end
end

net.Receive("npc_shop_dispense",function(len,client)
	local ID = net.ReadUInt(13)
	local shopkeeper = ents.GetByIndex(ID)
	if not shopkeeper then return end
	local Type = net.ReadString()
	local model = net.ReadString()
	shopkeeper:HandleDispenseRequest(client,Type,model)
end)