ENT.Base = "base_ai"
ENT.Type = "ai"

ENT.PrintName = "NPC Shopkeeper"
ENT.Author = "Spoffy (Gamer_cad)"
ENT.Information = "Base class for NPC shopkeepers."
ENT.Category = "Spoffy's Custom SENTs"

ENT.Spawnable = true
ENT.AdminSpawnable = false