include("shared.lua")
--
local function pass_item_back(npcid,class,model)
	if not npcid or not class or not model then ErrorNoHalt("NPC SHOP ERROR: Unable to pass item back. Missing arg.") return end
	net.Start("npc_shop_dispense")
	net.WriteUInt(npcid,13)
	net.WriteString(class)
	net.WriteString(model)	
	net.SendToServer()
end		

local function npc_shops_openmenu(name,modellist, npcid, xbsize, ybsize) --List of models/entities, sizes of buttons
	xbsize = xbsize or 80--Default arguments
	ybsize = ybsize or 80
	
	if not modellist then return end --No models, no menu!
	local window = vgui.Create("DFrame")
	local xSize = ScrW()/5 --Ratios for the frame size.
	local ySize = ScrH()/3
	if xSize < xbsize+10 then xSize = xbsize+10 end --Minimum frame size, else we can't fit in a button!
	if ySize < ybsize+35 then ySize = ybsize+35 end
	window:SetSize(xSize,ySize)
	local xPos = (ScrW() - window:GetWide())/2 --Position the screen right
	local yPos = ScrH()/3
	window:SetPos(xPos,yPos)
	window:SetSizable(false)
	window:SetTitle(name)
	
	local scrollpanel = vgui.Create("DScrollPanel",window) --The panel which we'll keep all the icons in.
	scrollpanel:SetSize(window:GetWide()-10,window:GetTall()-25)
	scrollpanel:SetPos(2,25)
	
	local iconsPerRow = math.floor((scrollpanel:GetWide()-30)/xbsize) -- Calculate how many icons we want per row.
	local xSpacing = ((scrollpanel:GetWide()-30) - (iconsPerRow*xbsize))/(iconsPerRow+1) --Calculate the spacing between the buttons.
	local ySpacing = 10 --Standard ySpacing.
	
	local icon_x = 0 --Handled in the loop.
	local icon_y = ySpacing --Start offset from the top. 
	
	if #modellist > 0 then
		for _,data in pairs(modellist) do --Class isn't strictly speaking correct, but type is taken
			local class = data[1]
			local model = data[2]
			local price = data[3]
			
			icon_x = icon_x + xSpacing
			if icon_x >= (iconsPerRow*(xSpacing + xbsize)) then
				icon_y = icon_y + ybsize + ySpacing
				icon_x = 0 + xSpacing
			end
			
			local icon = vgui.Create("SpawnIcon",scrollpanel)
			icon:SetPos(icon_x,icon_y)
			icon:SetSize(xbsize,ybsize)
			icon:SetModel(model)
			icon.EntModel = model
			icon.EntType = class --Custom definition here, so we know what type to spawn later.
			function icon:DoClick()
				--print("Model: " .. self.EntModel .. " Class: "..self.EntType.." EntID: " .. npcid) --DEBUG
				pass_item_back(npcid,self.EntType,self.EntModel)
			end
			
			local text = vgui.Create("DLabel",icon)
			text:SetFont("BudgetLabel")
			text:SetText("$" .. price)
			text:SetSize(7*string.len(text:GetText()),20)
			text:SetPos((icon:GetWide()-text:GetWide())/2,icon:GetTall()-text:GetTall())
			
			
			icon_x = icon_x + xbsize --Move the icon x on by the width of the icon, else we end up with them on top of each other.
		end
	else
		local text = vgui.Create("DLabel",window)
		text:SetText("This shopkeeper has nothing to sell you.")
		text:SizeToContents()
		text:SetPos((window:GetWide()-text:GetWide())/2,(window:GetTall()-text:GetTall())/2)
	end
	

	window:MakePopup()
end

net.Receive("npc_shops_openmenu",function(len)
	local name = net.ReadString()
	local items = net.ReadTable()
	local npcid = net.ReadUInt(13)
	if not items or not npcid then return end
	npc_shops_openmenu(name,items,npcid)
end)