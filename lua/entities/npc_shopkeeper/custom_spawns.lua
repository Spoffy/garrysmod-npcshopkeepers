ENT.CustomSpawnFunctions = {}

function ENT:CustomSpawn(class,model,ply)
	if self.CustomSpawnFunctions[class] then
		self.CustomSpawnFunctions[class](self,class,model,ply) --Passing 'self' manually, due to the way we're accessing the table.
		return true
	end
end


--[[ DECLARE CUSTOM SPAWN FUNCTIONS BELOW HERE, e.g:

ENT.CustomSpawnFunctions["weapon_smg1"] = function(class,model,ply)
	--Spawn SMG here
end

As is, prop_physics are exempt from this, use DispensePropPhysics() instead.
]]--

--This is an example, it is a (near) direct copy of the bouncy ball's spawn function.

ENT.CustomSpawnFunctions["sent_ball"] = function(self,class,model,ply)
	local ent = ents.Create( class )
	ent:SetPos( self:GetDispenseSpawnLoc() )
	ent:SetBallSize( math.random( 16, 48 ) )
	ent:Spawn()
	ent:Activate()
end

ENT.CustomSpawnFunctions["prop_vehicle_airboat"] = function(self,class,model,ply)
	local ent = ents.Create("prop_vehicle_airboat")
	ent:SetModel("models/airboat.mdl")
	ent:SetKeyValue("vehiclescript", "scripts/vehicles/airboat.txt")
	ent:SetPos(self:GetDispenseSpawnLoc())
	ent:Spawn()
	ent:Activate()
end