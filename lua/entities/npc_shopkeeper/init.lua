AddCSLuaFile("shared.lua")
AddCSLuaFile("cl_init.lua")

npc_shops = {}

include("shared.lua")
include("dispensing.lua")
include("saveload.lua")

local mf = {"male","female"}


function ENT:Initialize()
	self:SetModel( "models/humans/group0" .. math.random(1,3) .. "/" .. mf[math.random(1,2)] .. "_" .. "0"..math.random(1,7) .. ".mdl" ) -- Sets the model of the NPC.
	self:SetHullType( HULL_HUMAN ) -- Sets the hull type, used for movement calculations amongst other things.
	self:SetHullSizeNormal( )
	self:SetNPCState( NPC_STATE_SCRIPT )
	self:SetSolid(  SOLID_BBOX ) -- This entity uses a solid bounding box for collisions.
	self:CapabilitiesAdd(bit.bor(CAP_ANIMATEDFACE, CAP_TURN_HEAD )) -- Adds what the NPC is allowed to do ( It cannot move in this case ).
	self:SetUseType( SIMPLE_USE ) -- Makes the ENT.Use hook only get called once at every use.
	self:DropToFloor()
 
	self:SetMaxYawSpeed( 90 ) --Sets the angle by which an NPC can rotate at once.
	
	self:InitializeShop() --Let's set up shop!
	
	if not self.shop_name then --shop_name isn't a call here, as it's an internal check.
		ErrorNoHalt("NPC SHOP ERROR: Shopkeeper has no name!")
		self:Remove()
		return
	end
end

function ENT:SpawnFunction(ply,tr)
	if not tr.Hit then return end
	local ent = ents.Create("npc_shopkeeper")
	local spawnpos = nil
	if not tr.Hit then return end

	spawnpos = tr.HitPos
	ent:SetPos(spawnpos)
	ent:SetShopName("Shopkeeper") --This goes here as there's a check in initialize.
	ent:Spawn()
	ent:Activate()
	
	return ent
end

function ENT:OnTakeDamage(damage)
	return false
end

util.AddNetworkString("npc_shops_openmenu") --Interactivity code.

local function sendMenuRequest(ply,shop_name,items,npcid)
	if not ply or not items or not npcid then return end
	net.Start("npc_shops_openmenu")
	net.WriteString(shop_name)
	net.WriteTable(items)
	net.WriteUInt(npcid,13)
	net.Send(ply)
end

function ENT:AcceptInput(iName,activator,caller)
	if iName == "Use" then
		sendMenuRequest(activator,self.shop_name,self:GetSellList(),self:EntIndex())
	end
end

---------CON COMMANDS---------
local npc_shop_commands = {} -- We store the commands in here, makes them easier to add.

local function GetShopkeeper(ply) --Convinience function
	if not ply then return nil end
	local shopkeeper = ply:GetEyeTrace().Entity
	if not shopkeeper then
		ply:PrintMessage(HUD_PRINTCONSOLE, "You must be looking at a shopkeeper.")
		return
	end
	return shopkeeper
end

--Start custom commands--
function npc_shop_commands.getselling(ply,command,args,str)
	local shopkeeper = GetShopkeeper(ply)
	if not shopkeeper then return end
	local selling = shopkeeper:GetSellList()
	if not selling or #selling == 0 then
		ply:PrintMessage(HUD_PRINTCONSOLE, "That shopkeeper isn't selling anything.")
		return
	end
	for _,data in pairs(selling) do
		ply:PrintMessage(HUD_PRINTCONSOLE, "Type: "..data[1].."\t Model: "..data[2].."\t Price: "..data[3])
	end
end

function npc_shop_commands.clearselling(ply,command,args,str)
	local shopkeeper = GetShopkeeper(ply)
	if not shopkeeper then return end
	shopkeeper:ClearSellList()
	ply:PrintMessage(HUD_PRINTCONSOLE, "The shop has been cleared.")
end

function npc_shop_commands.removeitem(ply,command,args,str)
	local shopkeeper = GetShopkeeper(ply)
	if not shopkeeper then return end
	if #args < 2 then
		ply:PrintMessage(HUD_PRINTCONSOLE,'Insufficient arguments. The format is (Quotes are important): npcshops removeitem "class" (optional)"model"')
		return
	end
	if not shopkeeper:RemoveFromSellList{args[2],args[3]} then
		ply:PrintMessage(HUD_PRINTCONSOLE,"Unable to locate item. Please verify you have the correct type and model.")
		return
	end
	ply:PrintMessage(HUD_PRINTCONSOLE,"Item successfully removed.")
end

function npc_shop_commands.additem(ply,command,args,str)
	local shopkeeper = GetShopkeeper(ply)
	if not shopkeeper then return end
	if #args < 4 then
		ply:PrintMessage(HUD_PRINTCONSOLE,'Insufficient arguments. The format is (Quotes are important): npcshops additem "class" "displaymodel" price')
		return
	end
	if type(args[2]) ~= "string" then
		ply:PrintMessage(HUD_PRINTCONSOLE,"Argument "..args[2].." is of the incorrect type. It should be text.")
		return
	elseif type(args[3]) ~= "string" then
		ply:PrintMessage(HUD_PRINTCONSOLE,"Argument "..args[3].." is of the incorrect type. It should be text.")
		return
	elseif not tonumber(args[4]) then
		ply:PrintMessage(HUD_PRINTCONSOLE,"Argument "..args[4].." is of the incorrect type. It should be a number.")
		return
	end
	shopkeeper:AddToSellList{args[2],args[3],args[4]}
	ply:PrintMessage(HUD_PRINTCONSOLE,"Item successfully added. If there are still errors, you may have entered an incorrect type, model or price.")
end

function npc_shop_commands.savemap(ply,command,args,str)
	local file = npc_shops.save_npcs()
	if file then
		ply:PrintMessage(HUD_PRINTCONSOLE,"Save complete. All shopkeepers are now saved.")
	else
		ply:PrintMessage(HUD_PRINTCONSOLE,"Save failed. Please try again later.")
	end
end

function npc_shop_commands.loadmap(ply,command,args,str)
	local status = npc_shops.load_npcs()
	if status == "load_complete" then
		ply:PrintMessage(HUD_PRINTCONSOLE,"Load complete!")
	elseif status == "save_not_found" then
		ply:PrintMessage(HUD_PRINTCONSOLE,"Unable to find usable save for this map!")
	elseif status == "make_npc_failed" then
		ply:PrintMessage(HUD_PRINTCONSOLE,"NPC_SHOPS ERROR: Internal error, unable to create NPC.")
	end
end

function npc_shop_commands.setshopspawn(ply,command,args,str)
	local trace = ply:GetEyeTrace()
	if not trace.Hit then
		ply:PrintMessage(HUD_PRINTCONSOLE,"Please look at something and run this command again.")
		return
	end
	if not npc_shops.plysavestatus[ply:SteamID()] then
		if trace.Entity and trace.Entity:GetClass() == "npc_shopkeeper" then
			npc_shops.plysavestatus[ply:SteamID()] = trace.Entity
			ply:PrintMessage(HUD_PRINTCONSOLE,"Shopkeeper selected. Run this command again to set the spawn location to the location you are looking at.")
		else
			ply:PrintMessage(HUD_PRINTCONSOLE,"You are not targeting a shopkeeper. Please look at a shopkeeper and run this again.")
			return
		end
	else
		npc_shops.plysavestatus[ply:SteamID()]:SetDispenseSpawnLoc(trace.HitPos + Vector(0,0,20))
		npc_shops.plysavestatus[ply:SteamID()] = nil
		ply:PrintMessage(HUD_PRINTCONSOLE,"Spawn point set. Please remember to save!")
	end
end

function npc_shop_commands.setname(ply,command,args,str)
	local shopkeeper = GetShopkeeper(ply)
	if not shopkeeper then return end
	if #args < 2 then
		ply:PrintMessage(HUD_PRINTCONSOLE,"You need to specify a name.")
		return
	end
	shopkeeper:SetShopName(args[2])
	ply:PrintMessage(HUD_PRINTCONSOLE,"Name set.")
end


--End custom commands. Do not place new commands below this line unless you know what you're doing.--

function npc_shop_commands.help(ply,command,args,str) --Had to break the strings up due to Umesgs.
	ply:PrintMessage(HUD_PRINTCONSOLE, [[
	At the bottom of this message is the list of commands.]])
	
	ply:PrintMessage(HUD_PRINTCONSOLE,[[
	The format for adding to a shopkeeper is: "class" "model" price, E.g:
	"prop_physics" "model/humans/group01/male_01.mdl" 100 ]])
	
	ply:PrintMessage(HUD_PRINTCONSOLE,[[
	
	The format for removal is: "class" "model" or just "class", depending on how specific you need to be. 
	
	Below is a list of possible commands.]])
	for name,_ in pairs(npc_shop_commands) do
		ply:PrintMessage(HUD_PRINTCONSOLE, "\t"..name)
	end
end

local function handleShopCommand(ply,command,args,str)
	if not ply then return end
	if GAMEMODE_NAME == "darkrp" then
		if not ply:IsAdmin() or not ply:IsSuperAdmin() then return end
	end
	if npc_shop_commands[args[1]] then
		npc_shop_commands[args[1]](ply,command,args,str)
	else
		ply:PrintMessage(HUD_PRINTCONSOLE,"No such command found. Try 'npcshops help' for a list of commands. Note they are case sensitive.")
	end
end

concommand.Add("npcshops",handleShopCommand)

----------------------

scripted_ents.Register( ENT, "npc_shopkeeper")