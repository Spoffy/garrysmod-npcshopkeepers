npc_shops.saves = {}
npc_shops.plysavestatus = {}

function npc_shops.save_npcs()
	local shopkeepers = ents.FindByClass("npc_shopkeeper")
	npc_shop_saves = {}
	for _,shopkeeper in pairs(shopkeepers) do --Minimal data to recreate the NPCs.
		table.insert(npc_shop_saves,{shopkeeper:GetShopName(),shopkeeper:GetSellList(),shopkeeper:GetPos(),shopkeeper.DispenseSpawnPoint})
	end
	local savedata = util.TableToJSON(npc_shop_saves)
	if not file.Exists("NPC_SHOPS","DATA") or not file.IsDir("NPC_SHOPS","DATA") then
		file.CreateDir("NPC_SHOPS")
	end
	file.Write("NPC_SHOPS/"..game.GetMap()..".txt",savedata)
	return game.GetMap()..".txt"
end

function npc_shops.load_npcs()
	local save_string
	if file.Exists("NPC_SHOPS/"..game.GetMap()..".txt","DATA") then
		save_string = file.Read("NPC_SHOPS/"..game.GetMap()..".txt","DATA")
	end
	if not save_string then return "save_not_found" end --Let's use error codes!
	local save_table = util.JSONToTable(save_string)
	local clear_table = ents.FindByClass("npc_shopkeeper")
	for _,shopkeeper in pairs(clear_table) do
		shopkeeper:Remove()
	end
	for _,data in pairs(save_table) do
		local npc = ents.Create("npc_shopkeeper")
		if not npc then return "make_npc_failed" end
		npc:SetPos(data[3])
		npc:SetShopName(data[1] or "Shopkeeper")
		npc:Spawn()
		npc:Activate()
		
		npc:InitializeShop()
		npc:SetSellList(data[2])
		npc:SetDispenseSpawnLoc(data[4])
		
	end
	return "load_complete"
end

hook.Add("InitPostEntity", "npc_shop_mapload",npc_shops.load_npcs)
		
		
		